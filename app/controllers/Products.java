package controllers;

import play.mvc.*;

import services.SearsApi;
import views.html.*;

import java.util.*;


public class Products extends Controller {

  public static Result index() {
    List<String> myProduct = Arrays.asList("Ed", "John", "Peter", "Sam");
    return ok(productHome.render(null,"hello world I am here", 0));
  }

  public static Result search(String keyword) {
    String pageStr = request().getQueryString("page");
    Integer page = 0;
    if (pageStr != null) {
      try {
        page = Integer.parseInt(pageStr);
      } catch (NumberFormatException e) {
        e.printStackTrace();
      }
    }

    List<Map<String,String>> product1s = SearsApi.productSearch(keyword, page);
    return ok(productHome.render(product1s, "hello world I am here", page));
  }

  public static Result searchAjax(String keyword) {

    String pageStr = request().getQueryString("page");
    Integer page = 0;
    if (pageStr != null) {
      try {
        page = Integer.parseInt(pageStr);
      } catch (NumberFormatException e) {
        e.printStackTrace();
      }
    }

    List<Map<String, String>> product1s = SearsApi.productSearch(keyword, page);

    return ok(productItems.render(product1s, page));
  }
}