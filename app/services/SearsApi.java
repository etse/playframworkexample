package services;

import org.json.JSONArray;
import org.json.JSONObject;
import util.util.HttpUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by etse on 1/19/17.
 */
public class SearsApi {

  private static final String token = "0_21135_253402300799_1_2cad562f48bc0be5469fa29a79d4b6c5b0a7b1af77ef003c5e4bf371b135eb42";
  private static final String hash = "7caa962ddbf304386b7cb5d8018ba1c776d148ab98ba9596eb257e6c32e037c0";

  public static List<Map<String,String>> productSearch(String keyword, int page) {
    List<Map<String,String>> products = new ArrayList<>();
    try {
      //JSONObject obj = HttpUtil.send("https://sandboxplatform.shopyourway.com/products/search?q="+ keyword + "&category:19361;brands:Samsung;seller:sears.com&token=" + token + "&hash=" + hash, "GET", "");

      JSONObject obj = HttpUtil.send("https://sandboxplatform.shopyourway.com/products/search?limit=15&orderBy=relevancy&page=" + page+ "&q="+ keyword + "&seller:sears.com&token=" + token + "&hash=" + hash, "GET", "");

      JSONArray productsJson = obj.getJSONArray("products");
      for(int i = 0; i < productsJson.length(); i++) {
        JSONObject o = (JSONObject) productsJson.get(i);
        Map<String,String> newProduct = new HashMap<>();
        newProduct.put("name",o.getString("name"));
        newProduct.put("price",o.getBigDecimal("price").toString());

        newProduct.put("imageUrl",o.getString("imageUrl"));
        String description = o.getString("description");
        if (description.length() > 297) {
          description = description.substring(0, 297) + " ...";
        }
        newProduct.put("description",description);
        products.add(newProduct);
      }


    } catch (Exception b) {
      b.printStackTrace();
    }
    return products;
  }

}
