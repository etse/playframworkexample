package util.util;

import org.json.JSONObject;

import java.io.*;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * A Utility class to help with HTTP related work.
 * Created by etse on 12/3/16.
 */
public class HttpUtil {

  /**
   * Use for making HTTP request.
   *
   * @param url The target URL
   * @param type The TYPE of HTTP request.
   * @throws IOException When the method failed to complete the HTTP request.
   */
  public static void send(String url, String type) throws IOException {
    send(url, type, null);
  }

  /**
     * Use for making HTTP request.
     *
     * @param url The target URL
     * @param type The TYPE of HTTP request.
     * @param reqbody The request body of HTTP request.
     * @throws IOException When the method failed to complete the HTTP request.
     */
  public static JSONObject send(String url, String type, String reqbody) throws IOException {
    HttpURLConnection con = null;
    String result = null;

    con = getHttpConnection(url, type);
    //you can add any request body here if you want to post
    if (reqbody != null) {
      con.setDoInput(true);
      con.setDoOutput(true);
      DataOutputStream out = new DataOutputStream(con.getOutputStream());
      out.writeBytes(reqbody);
      out.flush();
      out.close();

    }
    con.connect();
    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
    String temp = null;
    StringBuilder sb = new StringBuilder();
    while ((temp = in.readLine()) != null) {
      sb.append(temp).append(" ");
    }
    result = sb.toString();
    System.out.println(result);
    in.close();

    return new JSONObject(result);
    //result is the response you get from the remote side
  }

  private static HttpURLConnection getHttpConnection(String url, String type) {
    URL uri = null;
    HttpURLConnection con = null;
    try {
      uri = new URL(url);
      con = (HttpURLConnection) uri.openConnection();
      con.setRequestMethod(type); //type: POST, PUT, DELETE, GET
      con.setDoOutput(true);
      con.setDoInput(true);
      con.setConnectTimeout(60000); //60 secs
      con.setReadTimeout(60000); //60 secs
      con.setRequestProperty("Accept-Encoding", "UTF-8");
    } catch (Exception e) {
      e.printStackTrace();
    }
    return con;
  }


  public static void main(String []args) {

  }
}
